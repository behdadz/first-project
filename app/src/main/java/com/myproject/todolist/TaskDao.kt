package com.myproject.todolist

import androidx.room.*

@Dao
interface TaskDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun add(task: Task):Long

    @Update
    fun update(task: Task):Int

    @Delete
    fun delete(task: Task):Int

    @Query("SELECT * FROM tbl_task ")
    fun getAll():MutableList<Task>

    @Query("DELETE FROM tbl_task")
    fun clearAll()

    @Query("SELECT * FROM tbl_task WHERE taskTitle LIKE '%'|| :query || '%' ")
    fun search(query:String):MutableList<Task>
}