package com.myproject.todolist

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

 class TodoListAdapter(private var tasks:MutableList<Task>,val eventListener: ItemEventListener): RecyclerView.Adapter<TodoListAdapter.TodoListViewHolder>() {


     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoListViewHolder {
         return TodoListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.task_item,parent,false))
     }

     override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
         holder.bindTask(tasks[position])
     }

     override fun getItemCount(): Int =tasks.size

     fun addTask(task: Task){
         tasks.add(0,task)
         notifyItemInserted(0)
     }

     fun clearAllTasks(){
         tasks.clear()
         notifyDataSetChanged()
     }

     fun updateTask(task: Task){
      for(i in 0 until tasks.size){
         if(task.id==tasks.get(i).id){
             tasks[i] = task
             notifyItemChanged(i)
         }
      }
     }

     fun deleteTask(task: Task){
      val index:Int=tasks.indexOf(task)
         if(index>-1){
             tasks.remove(task)
             notifyItemRemoved(index)
         }
     }

     fun setTasks(tasks:MutableList<Task>){
         this.tasks=tasks
         notifyDataSetChanged()
     }

     inner class TodoListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val taskCheckbox=itemView.findViewById<CheckBox>(R.id.checkBox_item_task)
         private val deleteBtnTask=itemView.findViewById<ImageView>(R.id.iv_item_delete)

         fun bindTask(task: Task){
             taskCheckbox.setOnCheckedChangeListener(null)
             if(task.completed == true){
                 taskCheckbox.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
             }else
                 taskCheckbox.paintFlags = 0

             taskCheckbox.isChecked= task.completed == true
             taskCheckbox.text = task.taskTitle
             taskCheckbox.setOnCheckedChangeListener { buttonView, isChecked ->
                 task.completed=isChecked
                 eventListener.onItemCheckedChange(task)

                 if (isChecked) {
                     taskCheckbox.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                 } else
                    taskCheckbox.paintFlags = 0

             }

             itemView.setOnLongClickListener {
                 eventListener.onLongItemClicked(task)
                 false
             }

             deleteBtnTask.setOnClickListener {
                 eventListener.onDeleteBtnClicked(task)
             }

         }

     }

     interface ItemEventListener{
         fun onLongItemClicked(task: Task)
         fun onDeleteBtnClicked(task: Task)
         fun onItemCheckedChange(task: Task)
     }


 }