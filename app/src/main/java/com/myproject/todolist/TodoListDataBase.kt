package com.myproject.todolist

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Task::class],exportSchema = false,version = 1)

abstract class TodoListDataBase:RoomDatabase (){
    abstract fun getDao():TaskDao

    companion object{

        private var todoListDataBase:TodoListDataBase ?= null

        fun getDatabase(context: Context):TodoListDataBase{
            if(todoListDataBase==null)
                todoListDataBase=Room.databaseBuilder(context.applicationContext,TodoListDataBase::class.java,"db_task")
                    .allowMainThreadQueries()
                    .build()
            return todoListDataBase!!
        }
    }
}