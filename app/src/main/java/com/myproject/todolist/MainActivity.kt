package com.myproject.todolist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity(),AddNewTaskFragment.AddNewTaskCallBack,TodoListAdapter.ItemEventListener
,EditTaskFragment.EditTaskCallBack{
    lateinit var recyclerView: RecyclerView
    lateinit var todoListAdapter: TodoListAdapter
    lateinit var taskDao: TaskDao
    lateinit var emptyStateImage: ImageView
    lateinit var tasks:MutableList<Task>
    lateinit var emptyStateTv:TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        taskDao=TodoListDataBase.getDatabase(this).getDao()
        emptyStateImage=findViewById(R.id.iv_emptyState)
        emptyStateTv=findViewById(R.id.tv_emptyState)
        recyclerView=findViewById(R.id.todoListRv)
        recyclerView.layoutManager=LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        tasks = taskDao.getAll()
        todoListAdapter=TodoListAdapter(tasks,this)
        recyclerView.adapter=todoListAdapter

        val searchTask=findViewById<EditText>(R.id.et_search)
        searchTask.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
               if(s?.isNotEmpty()!!){
                   val tasks=taskDao.search(s.toString())
                   todoListAdapter.setTasks(tasks)
               }else {
                   val tasks=taskDao.getAll()
                   todoListAdapter.setTasks(tasks)
               }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })

        val addNewTaskBtn=findViewById<View>(R.id.fab_addNewTask)
        addNewTaskBtn.setOnClickListener {
            val addNewTaskFragment=AddNewTaskFragment()
            addNewTaskFragment.show(supportFragmentManager,null)
        }

        val clearAllBtn=findViewById<ImageView>(R.id.clearAll)
        clearAllBtn.setOnClickListener {
            taskDao.clearAll()
            todoListAdapter.clearAllTasks()
            emptyStateImage.visibility=View.VISIBLE
            emptyStateTv.visibility=View.VISIBLE

        }
    }

    override fun onSaveButtonClicked(task: Task) {
        val taskId:Long=taskDao.add(task)
        if(taskId > -1){
            task.id=taskId
            todoListAdapter.addTask(task)
            emptyStateImage.visibility=View.GONE
            emptyStateTv.visibility=View.GONE
        }
    }

    override fun onLongItemClicked(task: Task) {
        val editTaskFragment=EditTaskFragment()
        val bundle=Bundle()
        bundle.putParcelable("task",task)
        editTaskFragment.arguments=bundle
        editTaskFragment.show(supportFragmentManager,null)
    }

    override fun onDeleteBtnClicked(task: Task) {
        val result:Int=taskDao.delete(task)
        if(result > 0){
            todoListAdapter.deleteTask(task)
        }
        if(tasks.size==0){
            emptyStateImage.visibility=View.VISIBLE
            emptyStateTv.visibility=View.VISIBLE
        }
    }

    override fun onItemCheckedChange(task: Task) {
        taskDao.update(task)
    }

    override fun saveTaskEditButtonClicked(task: Task) {
        val result:Int=taskDao.update(task)
        if(result>0){
            todoListAdapter.updateTask(task)
        }
    }

    override fun onStart() {
        super.onStart()
        if(tasks.size==0){
            emptyStateImage.visibility=View.VISIBLE
            emptyStateTv.visibility=View.VISIBLE

        }else{
            emptyStateImage.visibility=View.GONE
            emptyStateTv.visibility=View.GONE
        }


    }
}