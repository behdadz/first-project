package com.myproject.todolist

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class AddNewTaskFragment:DialogFragment() {
    lateinit var callBack: AddNewTaskCallBack
    override fun onAttach(context: Context) {
       if(context is AddNewTaskCallBack)
           callBack=context
        super.onAttach(context)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alertDialog=AlertDialog.Builder(context)
        val view:View=LayoutInflater.from(context).inflate(R.layout.add_new_task,null,false)
        val titleEt=view.findViewById<TextInputEditText>(R.id.et_add_task)
        val titleError=view.findViewById<TextInputLayout>(R.id.etl_add_task)
        val saveBtn=view.findViewById<MaterialButton>(R.id.btn_add_save)
        saveBtn.setOnClickListener {
            if(titleEt.length()>0) {
                val task=Task()
                task.taskTitle =titleEt.text.toString()
                task.completed =false
                callBack.onSaveButtonClicked(task)
                dismiss()
            }else
                titleError.error="title can not be empty"

        }

        return alertDialog.setView(view).create()
    }
    interface AddNewTaskCallBack{
        fun onSaveButtonClicked(task: Task)
    }
}