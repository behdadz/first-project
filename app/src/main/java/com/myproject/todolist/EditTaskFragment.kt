package com.myproject.todolist

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.DialogFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class EditTaskFragment:DialogFragment() {
    lateinit var callBack: EditTaskCallBack
    lateinit var task: Task
    override fun onAttach(context: Context) {
       if(context is EditTaskCallBack)
           callBack=context
        super.onAttach(context)
           task= arguments?.getParcelable<Task>("task")!!
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alertDialog=AlertDialog.Builder(context)
        val view:View=LayoutInflater.from(context).inflate(R.layout.edit_task,null,false)
        val titleEt=view.findViewById<TextInputEditText>(R.id.et_edit_task)
        titleEt.setText(task.taskTitle)
        val titleError=view.findViewById<TextInputLayout>(R.id.etl_edit_task)
        val saveBtn=view.findViewById<MaterialButton>(R.id.btn_edit_save)
        saveBtn.setOnClickListener {
            if(titleEt.length()>0) {
                task.taskTitle =titleEt.text.toString()
                callBack.saveTaskEditButtonClicked(task)
                dismiss()
            }else
                titleError.error="title can not be empty"

        }

        return alertDialog.setView(view).create()
    }
    interface EditTaskCallBack{
        fun saveTaskEditButtonClicked(task: Task)
    }
}