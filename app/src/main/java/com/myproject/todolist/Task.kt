package com.myproject.todolist

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "tbl_task")
@Parcelize
class Task
:Parcelable{
        @PrimaryKey(autoGenerate = true)
        var id:Long?=null

        var taskTitle:String?=null

        var completed:Boolean?=null
}